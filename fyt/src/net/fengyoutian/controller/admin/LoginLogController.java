package net.fengyoutian.controller.admin;

import com.jfinal.ext.route.ControllerBind;

import net.fengyoutian.controller.BaseController;
import net.fengyoutian.core.Constant;
import net.fengyoutian.model.User;
import net.fengyoutian.service.admin.LoginLogService;
import net.fengyoutian.util.SqlParaUtils;

/**
 * loginLog 控制层
 * 
 * @author 風佑兲(375910297@qq.com)
 * @date 2014年12月5日 上午10:05:56
 */
@ControllerBind(controllerKey = "/admin/loginLog")
public class LoginLogController extends BaseController {
	private LoginLogService loginLogService = new LoginLogService();
	
	public void list() {
		keepModel(User.class);
		User user = getModel(User.class);
		String startTime = SqlParaUtils.startTime(getPara("startTime"), getPara("endTime"));
		String endTime = SqlParaUtils.endTime(getPara("startTime"), getPara("endTime"));
		int pageNumber = getParaToInt(Constant.DWZ_PAGE_NUMBER, 1);
		int pageSize = getParaToInt(Constant.DWZ_PAGE_SIZE, Constant.PAGESIZE);
		setAttr("datas", loginLogService.getLoginLogPage(pageNumber, pageSize, user, startTime, endTime));
		render("list.html");
	}

	
}
